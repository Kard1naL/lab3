﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TRPO_Lab3.Lib;

namespace TRPO_lab_5.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Calculator()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Calculator(string p1, string p2, string a)
        {
            try
            {
                string result = (CL_S.Ploshad(Convert.ToDouble(p1), Convert.ToDouble(p2), Convert.ToDouble(a))).ToString();
                ViewData["result"] = result;
                return View("result");
            }
            catch (ArgumentException)
            {
                return View();
            }
        }
    }
}