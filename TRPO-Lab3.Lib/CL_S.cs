﻿using System;

namespace TRPO_Lab3.Lib
{
    public class CL_S
    {
        // переменные

        public static double p1 = 2;
        public static double p2 = 3;
        public static double a = 5;


        public double Formula(double p1, double p2, double a)
        {
            if (p1 < 0 || p2 < 0 || a < 0)
            {
                throw new ArgumentException();
            }
            else
            { 
            return 0.5 * (p1 + p2) * a;
            }
        }

        public static double Ploshad(double? p1, double? p2, double? a)
        {
            if (p2 <= 0 || p2 == null)
                throw new ArgumentException(nameof(p2));
            if (p1 <= 0 || p1 == null)
                throw new ArgumentException(nameof(p1));
            if (a <= 0 || a == null)
                throw new ArgumentException(nameof(a));
            return Convert.ToDouble(0.5 * (p1 + p2) * a);
        }
    }

}
