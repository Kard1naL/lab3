﻿using System;

namespace TRPO_Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            double p1;
            double p2;
            double a;
            Console.Write("Введите периметр нижнего основания правильной усеченной пирамиды (ABCDE):");
            p1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите периметр верхнего основания правильной усеченной пирамиды (abcde)):");
            p2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите апофему правильной усеченной пирамиды (OS):");
            a = Convert.ToDouble(Console.ReadLine());

            TRPO_Lab3.Lib.CL_S math = new TRPO_Lab3.Lib.CL_S();
            var result = math.Formula(p1, p2, a);

            Console.WriteLine("Результат: " + result);

            Console.ReadKey();
        }
    }
}
