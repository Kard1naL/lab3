using NUnit.Framework;
using TRPO_Lab3;
using System;


namespace Tests
{
    public class Tests
    {
        TRPO_Lab3.Lib.CL_S math = new TRPO_Lab3.Lib.CL_S();

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {


            double p1 = 2;
            double p2 = 3;
            double a = 5;
            const double expected = 12.5;

            var result = math.Formula(p1, p2, a);

            Assert.AreEqual(expected, result);


        }

        [Test]
        public void Test2()
        {
            double p1 = -2;
            double p2 = 3;
            double a = 5;

            Assert.Throws<ArgumentException>(() => math.Formula(p1, p2, a));
        }
    }
}