﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPO_Lab3.Lib;


namespace TRPO_Lab4.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double p1 = 3;
        double p2 = 2;
        double a = 1;
        public MainWindow()
        {
            InitializeComponent();
            raschet( p1,  p2,  a);
        }

        private void CheckIsNumeric(TextCompositionEventArgs e)
        {
            int result;

            if (!(int.TryParse(e.Text, out result) || e.Text == ","))
            {
                e.Handled = true;
            }
        }

        TRPO_Lab3.Lib.CL_S math = new TRPO_Lab3.Lib.CL_S();

        private void raschet(double p1, double p2, double a)
        {
            var result = math.Formula(p1, p2, a);

            tb_result.Text = Convert.ToString(Math.Round(result, 4));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                p1 = Convert.ToDouble(tb_per1.Text);
                p2 = Convert.ToDouble(tb_per2.Text);
                a = Convert.ToDouble(tb_apofema.Text);
            }

            catch
            {
                MessageBox.Show("Ошибка ввода исходных данных");
            }

            raschet(p1, p2, a);



        }



        private void tb_per1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            CheckIsNumeric(e);
            Button_Click(sender,e);
        }


        private void tb_per2_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            CheckIsNumeric(e);
            Button_Click(sender, e);

        }

        private void tb_apofema_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            CheckIsNumeric(e);
            Button_Click(sender, e);

        }


    }
}
